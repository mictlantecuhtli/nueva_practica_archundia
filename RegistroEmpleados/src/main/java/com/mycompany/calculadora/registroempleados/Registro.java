/*
Alumna: Fatima Azucena MC
Fecha: 01_11_2020
Correo: fatimaazucenamartinez274@gmail.com
 */
package com.mycompany.calculadora.registroempleados;

public class Registro {//Inicio clase principal
    public static void main(String[] args){//Inicio metodo principal
        //Creacion del objeto para poder mandar llamar a la clase que contiene
        //el formulario
        TablaDeRegistro objTablaDeRegistro = new TablaDeRegistro();
        objTablaDeRegistro.Tabla_Registro();
    }//Fin metodo principal
}//Fin clase principal
